package fibonacci;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Fibonacci {
    private int startInterval;
    private int endInterval;
    private int sumOdd = 0;
    private int sumEven = 0;
    private int numberOfOddNumbers = 0;
    private int numberOfEvenNumbers = 0;
    private Scanner in = new Scanner(System.in);

    private void enterInterval() {
        try {
            System.out.println("Enter start interval: ");
            startInterval = in.nextInt();
            System.out.println("Enter end interval: ");
            endInterval = in.nextInt();
            if (startInterval > endInterval) {
                System.out.println("A must be less then B");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. You have not entered a number.");
        }
    }

    public void printOddAndEven() {
        enterInterval();
        System.out.println("Odd numbers");
        for (int i = startInterval; i <= endInterval; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
        System.out.println("Even numbers");
        for (int i = endInterval; i > startInterval; i--) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    public void printSumOfOddAndEven() {
        enterInterval();
        System.out.println("Sum of odd numbers:");
        for (int i = startInterval; i <= endInterval; i++) {
            if (i % 2 == 1) {
                sumOdd += i;
            }
        }
        System.out.println(sumOdd);
        System.out.println("Sum of even numbers:");
        for (int i = startInterval; i <= endInterval; i++) {
            if (i % 2 == 0) {
                sumEven += i;
            }
        }
        System.out.println(sumEven);
    }

    public void buildFibonacci() {
        System.out.println("Enter size of set: ");
        int sizeOfSet = in.nextInt();
        int[] fibonacci = new int[sizeOfSet];
        fibonacci[0] = 0;
        fibonacci[1] = 1;
        System.out.println(fibonacci[0] + " " + fibonacci[1]);
        for (int i = 2; i < sizeOfSet; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            System.out.println(fibonacci[i]);
        }
            for (int fib : fibonacci) {
                if (fib % 2 == 1) {
                    numberOfOddNumbers++;
                }
            }
            int oddPercent = (numberOfOddNumbers * 100) / sizeOfSet;
            System.out.println(oddPercent + " % odd");
            for (int fib : fibonacci) {
                if (fib % 2 == 0) {
                    numberOfEvenNumbers++;
                }
            }
            int evenPercent = (numberOfEvenNumbers * 100) / sizeOfSet;
            System.out.println(evenPercent + " %even ");
    }
}



