package menu;

import constatnts.Constant;
import fibonacci.Fibonacci;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    private  Scanner in = new Scanner(System.in);
    private Fibonacci fibonacci = new Fibonacci();

    public  void menu() {
        while (true) {
            System.out.println(Constant.MENU);
            boolean validInput = false;
            while (!validInput) {
                System.out.print(Constant.ENTRY);
                try {
                    int option = in.nextInt();
                    if ((option < 0) || (option > 4)) {
                        System.out.println("\nPlease enter one of the following");
                        System.out.println("1, 2, 3, 4 or 0 ");
                    } else if (option == 1) {
                        fibonacci.printOddAndEven();
                    } else if (option == 2) {
                        fibonacci.printSumOfOddAndEven();
                    } else if (option == 3) {
                        fibonacci.buildFibonacci();
                    } else {
                        System.exit(0);
                    }
                } catch (InputMismatchException e) {
                    System.out.println("You have not entered a number.");
                }
            }
        }
    }
}








