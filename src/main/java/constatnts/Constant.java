package constatnts;

public class Constant {
    public static final String MENU = "\n--------------------------"
            + "\n|        MENU            |"
            + "\n--------------------------"
            + "\n1. Program prints odd numbers from start"
            + " to the end of interval and even from end to start; "
            + "\n2.Program prints the sum of odd and even numbers; "
            + "\n3. Program build fibonacci.Fibonacci numbers:"
            + " F1 will be the biggest odd number and F2 – the\n"
            + "biggest even number, user can enter the size of set (N)"
            + "0 to exit";

    public static final String ENTRY = "Please enter an Option:";
}
